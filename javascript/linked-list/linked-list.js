// used https://visualgo.net/en/list as a guidance for this exercise
// lesson learned : Abstract equality (==) will attempt to resolve the data types via type coercion before 
//                  making a comparison. Strict equality (===) will return false if the types are different. 
//                  console.log(3 == "3"); // true
//                  console.log(3 === "3"); // false.

'use strict';

class Node {
  constructor(val, prev, next){
    this.val = val;
    this.prev = prev;
    this.next = next;
  }  
}

// Queue ==  A -> B -> C
// Deque == A <-> B <-> C (on top of a different spelling)

class Deque {

  constructor() {
    this._count = 0;
    //first/last/prev/next are undefined
  }

  count() {
    return this._count;
  }

  // Work elements at the end of the list -> push & pop
  push(val) {
    let last = this.last;
    this.last = new Node(val, last, undefined);
    if (last)
       last.next = this.last;
    this._count++;
    if (this._count <= 1)
       this.first = this.last;
    return this;
  }

  pop() {
    if (!this._count) 
       return undefined;
    let last = this.last;
    this.last = last.prev;
    this._count--;
    if (this._count <= 1)
       this.first = this.last;
    return last.val;
  }

  // Work elements at the start of the list -> shift & unshift
  unshift(val) {
    let first = this.first;
    this.first = new Node(val, undefined, first);
    if (first)
       first.prev = this.first;
    this._count++;
    if (this._count === 1)
       this.last = this.first;
    return this;
  }

  shift() {
    if (!this._count)
       return undefined;
    let first = this.first;
    this.first = first.next;
    this._count--;
    if (this._count <= 1)
       this.last = this.first;
    return first.val;
  }

  delete(val) {
    let current = (this.last ? this.last : undefined);
    while (current) {
       if (current.val === val){
	      if (current.prev) current.prev.next = current.next;
	      if (current.next) current.next.prev = current.prev;
	      if (this.first === current) this.first = current.next;
	      if (this.last === current) this.last = current.prev;
	      this._count--;
	      return this;
       }
       current = current.prev;
    }    
    return this;
  }  
}

module.exports = function LinkedList(){
  return new Deque();
};