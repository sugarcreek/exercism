// Grains

// Calculate the number of grains of wheat on a chessboard given that the number
// on each square doubles.
// There are 64 squares on a chessboard.

// There once was a wise servant who saved the life of a prince. The king
// promised to pay whatever the servant could dream up. Knowing that the
// king loved chess, the servant told the king he would like to have grains
// of wheat. One grain on the first square of a chess board. Two grains on
// the next. Four on the third, and so on.

// Write code that shows:
// - how many grains were on each square, and
// - the total number of grains

/**
 * In JavaScript, integers beyond +/- 9007199254740991 cannot be accurately
 * represented. To see this in action, console.log() out the expected number
 * of grains on square #64:
 *
 * console.log(9223372036854775808);
 * // =>       9223372036854776000
 * //                         ^^^^
 *
 * This is because, in JavaScript, integers are represented as 64-bit floating
 * point numbers. If you want to learn more, see:
 *
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/MAX_SAFE_INTEGER
 * http://stackoverflow.com/questions/307179/what-is-javascripts-highest-integer-value-that-a-number-can-go-to-without-losin
 *
 * So, an accurate solution to this problem requires the use of a
 * "big integer" type. There are multiple ways to use big integer types.
 * We have provided you with BigInteger.js. You can read more about it here:
 *
 * https://github.com/peterolson/BigInteger.js
 * ^--- The "Methods" section of the README will be especially helpful.
 *
 * To get you started, this folder has a file of the big-integer module.
 * See its tests in this folder for a quick primer on how to use it! ( :
 */
 
 var BigInt = require('./big-integer');

var Grains = function() {
  const CHESSBOARD_NUMBER_OF_SQUARES = 64;
  const ERROR_RANGE = 'Square must be between 1 and ' + CHESSBOARD_NUMBER_OF_SQUARES;
  
  this.total = function() {
    // 2**0 + 2**1 + ... 2**(n-1) = (2**n) - 1
    return BigInt(2).pow(CHESSBOARD_NUMBER_OF_SQUARES).subtract(1).toString();
  };

  this.square = function(n) {
    if ( (n < 1) || (n > CHESSBOARD_NUMBER_OF_SQUARES) ) 
       throw new Error(ERROR_RANGE);
    return BigInt(2).pow(n-1).toString();
  };
};

module.exports = Grains;
