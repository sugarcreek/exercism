// Lesson learned : The test spec comes with pre-defined execeptions, which need implemented here
//                  Throw Error needs replaced with proper Exception
class BufferEmptyException extends Error {}
class BufferFullException extends Error {}
class BufferInvalidSizeException extends Error {}

//
// test spec also comes with clear() method, which needs implemented here
class CircularBuffer {

  clear() { this.stack = []; }

  constructor(size) {
     if (size < 0) 
        throw new BufferInvalidSizeException();
     this.max = size;
     this.stack = new Array(size)
  }

  write(val) {
     if (val == null) 
        return false;
     // if buffer is full, tell me
     if (this.stack.length === this.max) 
        throw new BufferFullException();
     this.stack.push(val);
     return true;
  }

  forceWrite(val) {
     // if buffer is full, get the oldest value out and put a new one in place
     if (this.stack.length === this.max) 
        this.stack.shift();
     return this.write(val);
  }

  read() {
     // if buffer empty, tell me
     if (this.stack.length === 0) 
        throw new BufferEmptyException();
     return this.stack.shift();
  }
}


module.exports = {
    circularBuffer: function circularBuffer(size){
        return new CircularBuffer(size);
    },
    bufferEmptyException: () => new BufferEmptyException(),
    bufferFullException: () => new BufferFullException(),
    bufferInvalidSizeException: () => new BufferInvalidSizeException()
};