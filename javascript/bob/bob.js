// Bob

// Bob is a lackadaisical teenager. In conversation, his responses are very limited.
// Bob answers 'Sure.' if you ask him a question.
// He answers 'Whoa, chill out!' if you yell at him.
// He says 'Fine. Be that way!' if you address him without actually saying anything.
// He answers 'Whatever.' to anything else.


var Bob = function() {};

Bob.prototype.hey = function(input) {
  if (input.match(/[A-Z]/) && input == input.toUpperCase()) {
    return 'Whoa, chill out!';
  } else if (input.slice(-1) == '?' || input.trimRight(', 0123456789').slice(-1) == '?') {
    return 'Sure.';
  } else if (input.trim() === '') {
    return 'Fine. Be that way!';
  } else {
    return 'Whatever.';
  }
};

module.exports = Bob;