
const WHEEL = 'abcdefghijklmnopqrstuvwxyz';
const RANDOM_KEY_LENGTH = 100

function Cipher(key) {
    if (key === '')
        throw new Error('Bad key');
    if (key && (key.toUpperCase() === key || /^\d+$/.test(key)))
        throw new Error('Bad key'); //error for all upper case or all digit key
    this.key = key || generateRandomKey();
}

Cipher.prototype.encode = function(text) {
    var characters = text.split('');
    var encodedChars = [];
    var self = this;
    characters.forEach(function(character,index) {
        var newIndex = WHEEL.indexOf(character) + WHEEL.indexOf(self.key[index]);
        if (newIndex >= WHEEL.length)
            newIndex -= WHEEL.length;
        encodedChars.push(WHEEL[newIndex]);
    });
    return encodedChars.join('');
};

Cipher.prototype.decode = function(cipher) {
    var characters = cipher.split('');
    var decodedChars = [];
    var self = this;
    characters.forEach(function(character,index) {
        var newIndex = WHEEL.indexOf(character) - WHEEL.indexOf(self.key[index]);
        if (newIndex < 0)
            newIndex += WHEEL.length;
        decodedChars.push(WHEEL[newIndex]);
    });
    return decodedChars.join('');
};

function generateRandomKey() {
    var i, randomKey = '';
    for (i = 0; i < RANDOM_KEY_LENGTH; i++) {
        randIndex = Math.floor(Math.random() * WHEEL.length);
        randomKey += WHEEL[randIndex];
    }
    return randomKey;
}

module.exports = Cipher;