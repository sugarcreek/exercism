var abbrevMatch = /(?:\b(\w)|([A-Z])[a-z]+)/g;

var Acronyms = {
  parse: function(text) {
    var matches = text.match(abbrevMatch);
    return matches.map(function(m) { return m[0].toUpperCase(); }).join("");
  }
};

module.exports = Acronyms;