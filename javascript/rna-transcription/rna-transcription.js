var DnaTranscriber = function () {};

DnaTranscriber.prototype.toRna = function (dna_strand) {
    var dna2rna = {'G': 'C', 'C': 'G', 'T': 'A', 'A': 'U'};
    var rna_strand = [];
    for (var x = 0; x < dna_strand.length; x++) {
        var nucleotide = dna_strand.charAt(x);
        if (nucleotide in dna2rna) {
            rna_strand.push(dna2rna[nucleotide]);
        } else {
            throw Error("Invalid input");
        }
    }
    return rna_strand.join('');
};

module.exports = DnaTranscriber;