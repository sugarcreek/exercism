var TwoFer = function () {};

TwoFer.prototype.twoFer_better = function (who = 'you') {
  return `One for ${who}, one for me.`;
};

TwoFer.prototype.twoFer = function (who) {
  // your code goes here
  // You will have to use the parameter who
  // in some way. In this example, it is just
  // returned, but your solution will have to
  // use a conditional.
  if (!who || 0 === who.length)
     who = 'you';
  str = 'One for '+ who + ', one for me.'
  return str;
};

module.exports = TwoFer;
