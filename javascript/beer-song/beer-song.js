var BeerSong = function() {}


BeerSong.prototype.verse = function( current ) {
    // special case verses
    var specialLines = {
        0: "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n",
        1: "1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n",
        2: "2 bottles of beer on the wall, 2 bottles of beer.\nTake one down and pass it around, 1 bottle of beer on the wall.\n",
    };
    // A normal verse
    var normalLine = "%d bottles of beer on the wall, %d bottles of beer.\nTake one down and pass it around, %d bottles of beer on the wall.\n";
    var sprintf = require('util').format;
    return current in specialLines ? specialLines[current] : sprintf(normalLine, current, current, current-1);
};

BeerSong.prototype.sing = function( start, stop) {
    stop || (stop = 0);
    var song = [];
    for (var verseNum = start; verseNum >= stop; verseNum--) {
        song.push(this.verse(verseNum));
    }
    return song.join("\n");
};


module.exports = BeerSong;