//Given an age in seconds, calculate how old someone would be on:

//   - Earth: orbital period 365.25 Earth days, or 31557600 seconds
//   - Mercury: orbital period 0.2408467 Earth years
//   - Venus: orbital period 0.61519726 Earth years
//   - Mars: orbital period 1.8808158 Earth years
//   - Jupiter: orbital period 11.862615 Earth years
//   - Saturn: orbital period 29.447498 Earth years
//   - Uranus: orbital period 84.016846 Earth years
//   - Neptune: orbital period 164.79132 Earth years

//So if you were told someone were 1,000,000,000 seconds old, you should
//be able to say that they're 31 Earth-years old.

//If you're wondering why Pluto didn't make the cut, go watch [this
//youtube video](http://www.youtube.com/watch?v=Z_2gbGXzFbs).

var SpaceAge = function(seconds) {
   var planetYears = function(earthYears) {
       var years = seconds / (31557600.0 * earthYears);
       // round to the nearest integer, then divide by 100 -> 2 decimals
       return Math.round(years * 100) / 100;
   };

   return {
       seconds: seconds,

       onEarth: function()   { return planetYears(1.0); },
       onMercury: function() { return planetYears(0.2408467); },
       onVenus: function()   { return planetYears(0.61519726); },
       onMars: function()    { return planetYears(1.8808158); },
       onJupiter: function() { return planetYears(11.862615); },
       onSaturn: function()  { return planetYears(29.447498); },
       onUranus: function()  { return planetYears(84.016846); },
       onNeptune: function() { return planetYears(164.79132); }
   };
};

module.exports = SpaceAge;