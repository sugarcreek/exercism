// Clean up user-entered phone numbers so that they can be sent SMS messages.
//The **North American Numbering Plan (NANP)** is a telephone numbering system used by many countries in North America like the United States, Canada or Bermuda. All NANP-countries share the same international country code: `1`.
//NANP numbers are ten-digit numbers consisting of a three-digit Numbering Plan Area code, commonly known as *area code*, followed by a seven-digit local number. The first three digits of the local number represent the *exchange code*, followed by the unique four-digit number which is the *subscriber number*.
//The format is usually represented as
//    (NXX)-NXX-XXXX
//where `N` is any digit from 2 through 9 and `X` is any digit from 0 through 9.
// For this exercise, the phone format is
//    (NXX) NXX-XXXX

var phoneNumber = function(number) {

  // Return a cleaned version of the number
  var cleanNumber = function(number) {
    // replace all non-digit chars
    number = number.replace(/\D/g, '');
    if (number.length != 10) {
       // cut off the international country code
       if (number.length == 11 && number[0] == '1') {
          // Use the last 10 digits of 11 digit number if it begins with 1
          number = number.slice(1);
       } else {
          //throw Error("Invalid phone number!");
          number = null;
       }
    } else {
       // area code has to start with 2<=number<=9
       if (!/^[2-9]\d+/.test(number))
          number = null;
       // exchange number has to start with 2<=number<=9
       if (!/^[2-9]\d{2}[2-9]\d+/.test(number))
          number = null;
    }
    return number;
  };

  // Precalculate these since the number doesn't change.
  this.clean_number = cleanNumber(number);
};


phoneNumber.prototype = {
  // Return the cleaned up digit string
  number: function() {
     return this.clean_number;
  },

  // Return the area code
  areaCode: function() {
     return this.clean_number.slice(0,3);  //slice(start, end) -> end not included
  },

  // Return the formatted phone number
  toString: function() {
     // (NXX) NXX-XXXX
     return ('(' + this.areaCode() + ') ' +
                   this.clean_number.slice(3,6) + '-' + this.clean_number.slice(6));
  }
};

module.exports = phoneNumber;