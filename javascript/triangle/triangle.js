// Triangle

// Determine if a triangle is equilateral, isosceles, or scalene.

// An _equilateral_ triangle has all three sides the same length.
// An _isosceles_ triangle has at least two sides the same length.
// A _scalene_ triangle has all sides of different lengths.

// Note
// For a shape to be a triangle at all, 
//     all sides have to be of length > 0, and 
//     the sum of the lengths of any two sides must be greater than or equal to the length of the third side. 
// See [Triangle Inequality](https://en.wikipedia.org/wiki/Triangle_inequality).

// Dig Deeper
// The case where the sum of the lengths of two sides _equals_ that of the 
// third is known as a _degenerate_ triangle - it has zero area and looks like 
// a single line. Feel free to add your own code/tests to check for degenerate triangles.

// Dang! Test spec does not allow for exceptions thrown in constructor

class Triangle {
  
  constructor(a, b, c) {

    this._kindof = 'unknown';
    this._errorflag = undefined;
    
    if ( a <= 0 || b <= 0 || c <= 0 )
       this._errorflag = new Error('All sides must have a positive value.');
       //throw new Error('All sides must have a positive value.');

    if ( a > (b + c) || b > (c + a) || c > (b + a) )
       this._errorflag = new Error('Invalid side values.');
       //throw new Error('Invalid side values.');

    this._kindof = 'scalene';
    if ( a == b || b == c || c == a )
       this._kindof = 'isosceles';
    if ( a == b && b == c )
       this._kindof = 'equilateral';
  }


  kind() {
    if (this._errorflag)
       throw this._errorflag;
    return this._kindof;
  }
};

module.exports = Triangle;