# Binary Search Tree
# Insert and search for numbers in a binary tree.

# A binary search tree consists of a series of connected nodes. Each node
# contains a piece of data (e.g. the number 3), a variable named `left`,
# and a variable named `right`. The `left` and `right` variables point at
# `nil`, or other nodes. Since these other nodes in turn have other nodes
# beneath them, we say that the left and right variables are pointing at
# subtrees. All data in the left subtree is less than or equal to the
# current node's data, and all data in the right subtree is greater than
# the current node's data.


class Bst
  attr_reader :data, :left, :right
  
  def initialize(root)
    @data = root
    @left = nil
    @right = nil
  end

  def insert(value)
    if value <= data
      left.insert(value) if left
      @left ||= Bst.new(value)
    else
      right.insert(value) if right
      @right ||= Bst.new(value)
    end
  end

  def each
    if block_given?
        left.each { |value| yield value } if left
        yield data
        right.each { |value| yield value } if right
    else
        enum_for(:each)
    end
  end
end

module BookKeeping
  VERSION = 1
end