# Sieve

# Use the Sieve of Eratosthenes to find all the primes from 2 up to a given number.
# The Sieve of Eratosthenes is a simple, ancient algorithm for finding all
# prime numbers up to any given limit. It does so by iteratively marking as
# composite (i.e. not prime) the multiples of each prime, starting with the multiples of 2.

# Create your range, starting at two and continuing up to and including the given limit. (i.e. [2, limit])

# The algorithm consists of repeating the following over and over:
#   - take the next available unmarked number in your list (it is prime)
#   - mark all the multiples of that number (they are not prime)
# Repeat until you have processed each number in your range.
# When the algorithm terminates, all the numbers in the list that have not been marked, are prime.

class Sieve
  attr_reader :primes

  def initialize(limit)
    @primes = generate_primes(limit)
  end

  private

  def generate_primes(limit)
    # create an array of candidate primes
    candidates = (2..limit).to_a
    primes = []
    # while there's still candidates left
    while candidates.any?
      # get the next candidate of the list (in the front)
      prime = candidates.shift
      # append prime to the list of primes
      primes << prime
      # throw away all multiples
      candidates.reject! { |candidate| candidate % prime == 0 }
    end
    # reply with the list of primes
    primes
  end
  
end

module BookKeeping
  VERSION = 1
end