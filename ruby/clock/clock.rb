# Clock
# Implement a clock that handles times without dates.
# You should be able to add and subtract minutes to it.
# Two clocks that represent the same time should be equal to each other.

class Clock
  def self.at(hour, minute)
    Clock.new(hour, minute)
  end

  def initialize(hour, minute)
    @minutes = hour * 60 + minute
  end

  def +(hour=0, minute)
    @minutes += hour * 60 + minute
    self
  end

  def ==(someclock)
    # compare with some other clock
    to_s == someclock.to_s
  end

  def hours
    # convert to hours
    @minutes / 60 % 24
  end

  def minutes
    # convert to minutes
    @minutes % 60
  end

  def to_s
    # pad with 0's
    "#{hours.to_s.rjust(2, '0')}:#{minutes.to_s.rjust(2, '0')}"
  end
end

module BookKeeping
  VERSION = 2
end