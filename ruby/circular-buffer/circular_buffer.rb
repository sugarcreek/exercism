class CircularBuffer
  BufferEmptyException = Class.new(StandardError)
  BufferFullException  = Class.new(StandardError)

  attr_reader :buffer, :size

  def clear(size = buffer.size)
    @buffer = Array.new(size)
    @size   = size
    @write  = 0
    @read   = 0
  end
  
  def initialize(size)
    clear(size)
  end

  def write(item)
    return if item.nil?
    raise BufferFullException unless buffer[@write].nil?
    write_buffer(item)
  end

  # In general, methods that end in ! indicate that the method will modify the object it's called on.
  # Ruby calls these as "dangerous methods" because they change state that someone else might have a reference to.
  def write!(item)
    return if item.nil?
    @read = ((@read + 1) % size) unless buffer[@write].nil?
    write_buffer(item)
  end

  # store item in buffer and position for the next write
  def write_buffer(item)
    buffer[@write] = item
    @write = ((@write + 1) % size)
  end

  def read
    raise BufferEmptyException if buffer.compact.empty?
    item = buffer[@read]
    buffer[@read] = nil
    @read = ((@read + 1) % size)
    item
  end

end