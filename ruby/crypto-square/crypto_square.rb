class Crypto

  def initialize(text)
    @text = text
  end

  def normalize_plaintext
    @text.downcase.gsub(/[^a-z0-9]/,"")
  end

  def size
    Math.sqrt(normalize_plaintext.length).ceil
  end

  def plaintext_segments
    normalize_plaintext.scan(/.{1,#{size}}/)
  end

  def ciphertext
    normalize_ciphertext.gsub(" ","") 
  end

  def normalize_ciphertext
   text = ""
    (0..size-1).each do |i|
      text += plaintext_segments.map { |s| s[i] }.compact.join + " "
    end
    text.rstrip
  end

end

module BookKeeping
  VERSION = 1
end