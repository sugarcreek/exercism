## Flatten Array
# Take a nested list and return a single flattened list with all values except nil/null.
# The challenge is to write a function that accepts an arbitrarily-deep nested list-like structure and returns a flattened structure without any nil/null values.
# Example :
# input: [1,[2,3,nil,4],[nil],5]
# output: [1,2,3,4,5]

class FlattenArray

    def self.flatten(input, out=[])
        input.each do |i|
            if i.kind_of?(Array)
                flatten(i, out)
            else
                out.push(i) if i != nil
            end
        end
        out
    end  
end

module BookKeeping
  VERSION = 1
end