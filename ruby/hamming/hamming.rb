class Hamming
    def self.compute(strand_a, strand_b)
        raise ArgumentError.new('Strands have different lengths.') unless strand_a.length == strand_b.length
        distance = 0
        strand_b_char = strand_b.split("")
        strand_a.each_char.with_index(0) do |char, idx|
          unless strand_b_char[idx] == char.to_s
            distance += 1
          end
        end
        distance
    end
end