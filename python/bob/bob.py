def hey(phrase):
    chars = list(phrase.rstrip(' '))
    length = len(phrase.rstrip(' '))
    if (length and chars[length-1] == '?'):  # [:-1] doesn't work here
        if (phrase.strip(' ABCDEFGHIJKLMNOPQRSTUVWXYZ') == '?'):
            return 'Whoa, chill out!'
        else:
            return 'Sure.'
    elif (phrase.strip(' \t\n\r') == ''):
        return 'Fine. Be that way!'
    elif (phrase == phrase.upper()):
        if (phrase.strip(', 0123456789') == ''):
            return 'Whatever.'
        else:
            return 'Whoa, chill out!'
    else:
        return 'Whatever.'
