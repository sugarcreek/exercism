def hello(name=''):
    if not name:
        return 'Hello, World!'
    else:
        return name


print(hello())
