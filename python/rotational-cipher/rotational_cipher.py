def rotate(text, key):

    """
    The Caesar cipher is a simple shift cipher that relies on transposing all the letters
    in the alphabet using an integer key between `0` and `26`.
    """
    MAX_KEY_SIZE = 26
    if key > MAX_KEY_SIZE or key < 0:
        raise ValueError('Key has invalid value: {}'.format(key))
    translated = ''
    for char in text:
        if char.isalpha():
            num = ord(char)             # integer representation of char
            translate_to = num + key    # integer representation of what we want
            # text can be mix of upper and lower case characters
            if char.isupper():
                if translate_to > ord('Z'):
                    translate_to -= MAX_KEY_SIZE    # circle around
                elif num < ord('A'):
                    translate_to += MAX_KEY_SIZE    # start at the back
            else:   # char.islower(): must be true
                if translate_to > ord('z'):
                    translate_to -= MAX_KEY_SIZE    # circle around
                elif num < ord('a'):
                    translate_to += MAX_KEY_SIZE    # start at the back
            translated += chr(translate_to)
        else:
            # just add the character to the encoded string as is
            translated += char
    return translated
