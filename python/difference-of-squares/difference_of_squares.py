
# learned something new : function annotations, which remind me of Pascal features
# function annotations covered in PEP 3107 (http://www.python.org/dev/peps/pep-3107/)
# Specifically, the -> marks the return function annotation.
# found explanation here : https://stackoverflow.com/questions/14379753/what-does-mean-in-python-function-definitions

# I think these annotations are awesome, so I'm using them here :-)

def square_of_sum(num: int) -> int:
    return sum(range(1, num + 1)) ** 2


def sum_of_squares(num: int) -> int:
    return sum(map(lambda x: x**2, range(1, num + 1)))


def difference(num: int) -> int:
    return square_of_sum(num) - sum_of_squares(num)
