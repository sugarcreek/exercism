def is_isogram(string):
    chars = list(string.lower())
    hash = {}
    for x in chars:
        if(x != ' ' and x != '-'):
            if x in hash:
                return False
            else:
                hash[x] = x
    return True

# print(is_isogram('accentor'))
