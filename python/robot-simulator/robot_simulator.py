# Globals for the bearings
NORTH, EAST, SOUTH, WEST = 0, 1, 2, 3
# moves on the X-axis are East and West, with West opposite of East
X_AXIS = [0, 1, 0, -1]  # N, E, S, W
# moves on the Y-axis are North and South, with South opposite of North
Y_AXIS = [1, 0, -1, 0]  # N, E, S, W


class Robot(object):
    def __init__(self, bearing=NORTH, x=0, y=0):
        self.bearing = bearing
        self.x = x
        self.y = y
        self.movements = {
            'A': self.advance,
            'R': self.turn_right,
            'L': self.turn_left
        }

    @property
    def coordinates(self):
        return self.x, self.y

    def turn_right(self):
        self.bearing = (self.bearing + 1) % 4

    def turn_left(self):
        self.bearing = (self.bearing - 1) % 4

    def advance(self):
        self.x += X_AXIS[self.bearing]
        self.y += Y_AXIS[self.bearing]

    def simulate(self, directions):
        for direction in directions:
            self.movements[direction]()
