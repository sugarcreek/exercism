# Determine if a triangle is equilateral, isosceles, or scalene.

# An _equilateral_ triangle has all three sides the same length.

# An _isosceles_ triangle has at least two sides the same length. (It is sometimes
# specified as having exactly two sides the same length, but for the purposes of
# this exercise we'll say at least two.)

# A _scalene_ triangle has all sides of different lengths.

# #### What is a triangle ###
# For a shape to be a triangle at all, all sides have to be of length > 0, and
# the sum of the lengths of any two sides must be greater than or equal to the
# length of the third side.
# See Triangle Inequality: https://en.wikipedia.org/wiki/Triangle_inequality


class TriangleError(Exception):
    pass


class Triangle:
    EQUILATERAL = 'equilateral'
    ISOSCELES = 'isosceles'
    SCALENE = 'scalene'

    side_a = side_b = side_c = 0

    def __init__(self, side_a, side_b, side_c):
        if (side_a + side_b <= side_c or side_b + side_c <= side_a or side_a + side_c <= side_b):
            # if any of the sides is <= 0, we'll end up here as well, so that case is covered
            raise TriangleError('Does not satisfy the Triangle inequality')
        self.side_a = side_a
        self.side_b = side_b
        self.side_c = side_c

    def is_equilateral(self):
        return self.side_a == self.side_b == self.side_c

    def is_isosceles(self):
        return self.side_a == self.side_b or self.side_b == self.side_c or self.side_a == self.side_c

    def kind(self):
        if self.is_equilateral():
            return self.EQUILATERAL
        if self.is_isosceles():
            return self.ISOSCELES
        return self.SCALENE
