def saddle_points(matrix):
    """
    Detect saddle points in a matrix.

    ```text
            0  1  2
          |---------
        0 | 9  8  7
        1 | 5  3  2     <--- saddle point at (1,0)
        2 | 6  6  7
    ```
    It's called a "saddle point" because
    it is greater than or equal to every element in its row
    and
    less than or equal to every element in its column.
    """
    if not all(len(row) == len(matrix[0]) for row in matrix):
        raise ValueError("Irregular matrix")

    rowmax = [max(row) for row in matrix]
    colmin = [min(col) for col in zip(*matrix)]

    saddles = set()
    for m, row in enumerate(matrix):
        for n, item in enumerate(row):
            if item == rowmax[m] and item == colmin[n]:
                saddles.add((m, n))
    return saddles
