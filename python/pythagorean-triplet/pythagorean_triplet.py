from itertools import combinations
from math import gcd
# python2 : from fractions import gcd
from math import ceil


def primitive_triplets(number_in_triplet):
    if number_in_triplet % 4 != 0:
        raise ValueError("Must start with a multiple of 4")

    # Working with a=(m^2-n^2), b=2*m*n and c=(m^2+n^2)
    # and taking the starter value to be b
    product = int(number_in_triplet / 2)
    limit = int(ceil(product ** 0.5))

    # (m, n) multiple pairs
    pairs = (
        (x, product // x)
        for x in range(1, limit) if product % x == 0
    )

    return {
        make_triplet(m, n)
        for (m, n) in filter(coprime, pairs)
    }


def triplets_in_range(range_start, range_end):
    return set(filter(is_triplet, combinations(range(range_start, range_end+1), 3)))


def is_triplet(triplet):
    a, b, c = sorted(triplet)
    # math.pow() returns a float, we don't want that
    return a**2 + b**2 == c**2


# A method for finding all primitive pythagorean triplet is given in wikipedia
# (http://en.wikipedia.org/wiki/Pythagorean_triple#Generating_a_triple). The
# triplet a=(m^2-n^2), b=2*m*n and c=(m^2+n^2), where m and n are coprime and
# m-n>0 is odd, generate a primitive triplet. Note that this implies that b has
# to be divisible by 4 and a and c are odd. Also note that we may have either
# a>b or b>a.
def make_triplet(m, n):
    """
    Make a pythagorean triplet from coprime integers m and n
    using a=(m^2-n^2), b=2*m*n and c=(m^2+n^2)
    """
    a, b, c = sorted((
        abs(m**2 - n**2),
        2 * m * n,
        m**2 + n**2
    ))
    return (a, b, c)


def coprime(pair):
    """
    Are m and n coprime?
    """
    m, n = pair
    return gcd(m, n) == 1
