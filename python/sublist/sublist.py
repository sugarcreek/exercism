# Sublist
# Given two lists, determine if
#    the first list is contained within the second list,
#    the second list is contained within the first list,
#    both lists are contained within each other or
#    none of these are true.
# Specifically, a list A is a sublist of list B if by dropping 0 or more elements
# from the front of B and 0 or more elements from the back of B you get a list
# that's completely equal to A.

# Examples:
# A = [1, 2, 3], B = [1, 2, 3, 4, 5], A is a sublist of B
# A = [3, 4, 5], B = [1, 2, 3, 4, 5], A is a sublist of B
# A = [3, 4], B = [1, 2, 3, 4, 5], A is a sublist of B
# A = [1, 2, 3], B = [1, 2, 3], A is equal to B
# A = [1, 2, 3, 4, 5], B = [2, 3, 4], A is a superlist of B
# A = [1, 2, 4], B = [1, 2, 3, 4, 5], A is not a superlist of, sublist of or equal to B

# these values are imported into the test spec, so it's assigned values don't really matter
SUBLIST, SUPERLIST, EQUAL, UNEQUAL = 1, 2, 3, 4


def check_lists(first_list, second_list):
    if first_list == second_list:
        return EQUAL

    if len(first_list) > len(second_list):
        return SUPERLIST if is_sublist(second_list, first_list) else UNEQUAL

    if len(first_list) < len(second_list):
        return SUBLIST if is_sublist(first_list, second_list) else UNEQUAL

    return UNEQUAL


def is_sublist(l1, l2):
    # determine how many positions we need to check
    for val in range(len(l2) - len(l1) + 1):
        # for each position, check if the other list fits or matches
        if l2[val:val+len(l1)] == l1:
            return True
    return False
