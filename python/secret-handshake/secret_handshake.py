from functools import reduce


ACTIONS = [('wink', 0b1), ('double blink', 0b10), ('close your eyes', 0b100), ('jump', 0b1000)]
REVERSE = 0b10000


def handshake(number):
    if isinstance(number, str):
        try:
            number = int(number, 2)  # convert to binary number (base=2)
        except ValueError:
            number = 0
    number = max(number, 0)
    ops = [
        action
        for action, code in ACTIONS if number & code
    ]
    if number & REVERSE:
        return list(reversed(ops))
    else:
        return ops


def code(secret_code):
    action_lookup = dict(ACTIONS)
    try:
        codes = [action_lookup[action] for action in secret_code]
    except KeyError:
        codes = [0]

    code = reduce(lambda c, op: c | op, codes)  # 11 is 1 | 10

    # Detect reverse order of operations
    if len(codes) > 1 and codes[0] > codes[1]:
        code |= REVERSE

    return bin(code)[2:]
