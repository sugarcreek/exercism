# Prime Factors
# Compute the prime factors of a given natural number.
# A prime number is only evenly divisible by itself and 1.
# Note that 1 is not a prime number.
# #Example
# What are the prime factors of 60?
# - Our first divisor is 2. 2 goes into 60, leaving 30.
# - 2 goes into 30, leaving 15.
#   - 2 doesn't go cleanly into 15. So let's move on to our next divisor, 3.
# - 3 goes cleanly into 15, leaving 5.
#   - 3 does not go cleanly into 5. The next possible factor is 4.
#   - 4 does not go cleanly into 5. The next possible factor is 5.
# - 5 does go cleanly into 5.
# - We're left only with 1, so now, we're done.

# Our successful divisors in that computation represent the list of prime
# factors of 60: 2, 2, 3, and 5 as 2 * 2 * 3 * 5 = 60

# ran this using python3 : OK
# ran this using python2 :
# ERROR: test_factors_include_a_large_prime (__main__.PrimeFactorsTest)
# ----------------------------------------------------------------------
# Traceback (most recent call last):
#  File "prime_factors_test.py", line 28, in test_factors_include_a_large_prime
#    self.assertEqual(prime_factors(93819012551), [11, 9539, 894119])
#  File "/home/devops/Projects/exercism/python/prime-factors/prime_factors.py", line 23, in prime_factors
#    for p in range(2, natural_number+1):
#  MemoryError


def prime_factors(natural_number):
    if natural_number == 1:
        return []
    for p in range(2, natural_number+1):
        if natural_number % p == 0:
            return [p] + prime_factors(natural_number // p)  # floor division, whole number adjusted to the left
