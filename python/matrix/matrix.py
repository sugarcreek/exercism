# python2 version
# I wrote python2 code and ran it in python3, hence some map issues (which I was not aware of)
# Thanks : https://stackoverflow.com/questions/6800481/python-map-object-is-not-subscriptable


class Matrix_v2(object):
    def __init__(self, matrix_string):
        # list of rows
        self.rows = [
            # 2. and split each row into column integers
            map(int, row.split())
            # 1. split string in rows
            for row in matrix_string.split('\n')
        ]
        # list of columns by mapping tuples
        self.columns = map(list, zip(*self.rows))


# python3 version
# map(int, row.split()) will result in
#   TypeError: 'map' object is not subscriptable because
# In Python 3, map returns an iterable object of type map, and not a subscriptible list
# Fix : wrap the map in a list()
class Matrix(object):
    def __init__(self, matrix_string):
        # list of rows
        self.rows = [
            # 2. and split each row into column integers
            list(map(int, row.split()))
            # 1. split string in rows
            for row in matrix_string.split('\n')
        ]
        # list of columns by mapping tuples
        self.columns = list(map(list, zip(*self.rows)))