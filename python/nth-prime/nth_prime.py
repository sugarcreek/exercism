# Nth Prime
# Given a number n, determine what the nth prime is.
# By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that
# the 6th prime is 13.
# If your language provides methods in the standard library to deal with prime
# numbers, pretend they don't exist and implement them yourself.

from math import sqrt


def nth_prime(positive_number):
    if positive_number == 0:
        raise ValueError('Zero(0) is not a prime number')
    num = 2
    prime_count = 1

    while prime_count < positive_number:
        num += 1
        if is_prime(num):
            prime_count += 1
    return num


def is_prime(current):
    if current == 2:
        return True
    if current < 2 or current % 2 == 0:
        return False
    return not any(current % i == 0 for i in range(3, int(sqrt(current)) + 1, 2))
