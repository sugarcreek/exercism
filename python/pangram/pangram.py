def is_pangram(sentence):
    keys = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    zeros = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    # pangram = {key: value for key, value in zip(keys, 0)} -> doesn't work - zip param2 needs to be iterable
    pangram = {key: value for key, value in zip(keys, zeros)}
    chars = list(sentence.lower())
    for char in chars:  # skip non-alphabetical, but still ASCII chars
        if(char in keys):
            pangram[char] = 1
    if 0 in pangram.values():
        return False
    else:
        return True


print(is_pangram('"Five quacking Zephyrs jolt my wax bed."'))
