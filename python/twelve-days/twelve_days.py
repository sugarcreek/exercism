PRESENTS = {
    1: "a Partridge in a Pear Tree",
    2: "two Turtle Doves",
    3: "three French Hens",
    4: "four Calling Birds",
    5: "five Gold Rings",
    6: "six Geese-a-Laying",
    7: "seven Swans-a-Swimming",
    8: "eight Maids-a-Milking",
    9: "nine Ladies Dancing",
    10: "ten Lords-a-Leaping",
    11: "eleven Pipers Piping",
    12: "twelve Drummers Drumming",
}

NUMERALS = {
    1: 'first',
    2: 'second',
    3: 'third',
    4: 'fourth',
    5: 'fifth',
    6: 'sixth',
    7: 'seventh',
    8: 'eighth',
    9: 'ninth',
    10: 'tenth',
    11: 'eleventh',
    12: 'twelfth',
}


def verse(day_number):
    lines = ["On the %s day of Christmas my true love gave to me" % NUMERALS[day_number]]
    # Add all the gifts except for day 1
    lines.extend(PRESENTS[i] for i in range(day_number, 1, -1))
    # Day 1 (last line) might have an 'and'
    lines.append(PRESENTS[1] if day_number == 1 else 'and %s' % PRESENTS[1])
    return ', '.join(lines) + '.\n'


def verses(start, end):
    return '\n'.join(verse(cnt) for cnt in range(start, end+1)) + '\n'


def sing():
    return verses(1, 12)
