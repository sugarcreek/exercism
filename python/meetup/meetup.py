import datetime


def meetup_day(year, month, day_of_the_week, which):
    mapping = {'1st': 0, '2nd': 1, '3rd': 2, '4th': 3, '5th': 4, 'last': -1}
    dow = {'Monday': [], 'Tuesday': [], 'Wednesday': [], 'Thursday': [], 'Friday': [], 'Saturday': [], 'Sunday': []}

    # Store the days of the month by their corrsponding day of the week
    dt = datetime.date(year, month, 1)
    while dt.month == month:
        day_name = dt.strftime("%A")  # aaa
        """
        put date in corresponding weekday list
        1st of the month will be in index 0, 2nd in index 1, 3rd in index 3, last in last_index-1
        """
        dow[day_name].append(dt.day)
        dt = dt + datetime.timedelta(days=1)  # rinse, repeat -> next day

    # ...then get the right day
    if which in mapping:
        index = mapping[which]
        day = dow[day_of_the_week][index]

    if which == 'teenth':
        day = [d for d in dow[day_of_the_week] if 12 < d < 20][0]

    return datetime.date(year, month, day)
