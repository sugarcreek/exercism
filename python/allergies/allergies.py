class Allergies(object):

    items = {
        'eggs': 1,
        'peanuts': 2,
        'shellfish': 4,
        'strawberries': 8,
        'tomatoes': 16,
        'chocolate': 32,
        'pollen': 64,
        'cats': 128
        }

    def __init__(self, score):
        self.score = score

    def is_allergic_to(self, item):
        return bool(self.score & self.items[item])

    @property
    def lst(self):
        return [item for item in self.items.keys() if self.is_allergic_to(item)]
