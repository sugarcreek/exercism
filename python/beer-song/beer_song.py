def verse(current):
    """
    Sing a specific verse of '99 Bottles of Beer on the Wall'.
    current: The verse to sing
    """
    templates = {
        '_': '{0} bottles of beer on the wall, {0} bottles of beer.\n'
             'Take one down and pass it around, {1} bottles of beer on the wall.\n',

        '2': '{0} bottles of beer on the wall, {0} bottles of beer.\n'
             'Take one down and pass it around, {1} bottle of beer on the wall.\n',

        '1': '{0} bottle of beer on the wall, {0} bottle of beer.\n'
             'Take it down and pass it around, no more bottles of beer on the wall.\n',

        '0': 'No more bottles of beer on the wall, no more bottles of beer.\n'
             'Go to the store and buy some more, 99 bottles of beer on the wall.\n'
    }
    next = current - 1
    return templates['_' if current > 2 else str(current)].format(current, next)


def song(number1, number2=0):
    """
    Sing a range of verses of '99 Bottles of Beer on the Wall'.
    number1: The verse to start with
    number2: The verse to end with, defaults to the 0th verse
    """
    return '\n'.join([verse(i) for i in range(number1, number2-1, -1)]) + '\n'
