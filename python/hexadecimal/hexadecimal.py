# Hexadecimal

# Convert a hexadecimal number, represented as a string (e.g. "10af8c"), to its decimal equivalent using first principles
# On the web we use hexadecimal to represent colors, e.g. green: 008000, teal: 008080, navy: 000080).
# The program should handle invalid hexadecimal strings.


def hexa(hex_string):
    # check if hex_string has illegal chars
    hex_dict = dict(zip("0123456789abcdef", range(16)))
    dec_num = 0
    for char in hex_string.lower():
        dec_num <<= 4
        try:
            dec_num |= hex_dict[char]
        except KeyError:
            raise ValueError
    return dec_num
