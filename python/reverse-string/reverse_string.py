# Reverse String
# For example:
# input: "cool"
# output: "looc"


def reverse(input=''):
    return input[::-1]
