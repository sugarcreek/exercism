def distance(strand_a, strand_b):
    nucleo_a = list(strand_a)
    nucleo_b = list(strand_b)
    if (len(nucleo_a) != len(nucleo_b)):
        raise ValueError('strands have different length: length strand a={}, length strand b={}'.format(len(nucleo_a), len(nucleo_b)))
    else:
        distance = 0
        for idx, elem in enumerate(nucleo_a):
            if (nucleo_a[idx] != nucleo_b[idx]):
                distance += 1
        return distance
