import re


# filter crap out of words
# 1. only replace ' when there's at least 2 of 'em
# 2. remove !!&@$%^& chars
def clean_word(word):
    remove_chars = ('!', '@', '$', '%', '^', '&')
    chars = list(word)
    newword = list()
    if word.count("'") and word.count("'") % 2 == 0:
        newword = word.replace('\'', '')
    else:
        for char in chars:
            if char not in remove_chars:
                newword.append(char)
    return ''.join(newword)


def word_count(phrase):
    words = re.split(r'[;,_:\.\-\s]\s*', phrase)
    word_count = {}
    for word in words:
        if word == '':
            continue
        word = clean_word(word)
        if word.lower() in word_count:
            word_count[word.lower()] += 1
        else:
            word_count[word.lower()] = 1
    return word_count
