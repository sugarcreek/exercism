def sieve(limit):
    """
    Return a list of prime numbers up to the given limit using the Sieve of Eratosthenes
    """
    primes = []
    # list of unordered, but unique values, no doubles
    non_primes = set()

    for i in range(2, limit+1):
        if i not in non_primes:
            primes.append(i)
            # add multiples of the current iteration values to non_primes
            # if the value already exist, don't add it -> use a set
            for a in range(i*i, limit+1, i):
                # 'add' works on sets only
                non_primes.add(a)
    return primes


# print(sieve(6))
