def to_rna(dna_strand):
    # key is nucleotide, value is its complement (rna strand)
    dna2rna = {'G': 'C', 'C': 'G', 'T': 'A', 'A': 'U'}
    nucleotides = list(dna_strand)
    rna_strand = list()
    for nucleotide in nucleotides:
        if nucleotide in dna2rna:
            rna_strand.append(dna2rna[nucleotide])
        else:
            raise ValueError('could not find {} in {}'.format(nucleotide, dna2rna.keys()))
    return ''.join(rna_strand)
