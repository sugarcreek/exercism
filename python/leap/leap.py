def is_leap_year(year):
    """Determine whether a year is a leap year."""
    return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)


def is_leap_year_otherway(year):
    if year % 4 == 0:
        if year % 100 == 0:
            if year % 400 == 0:
                return True
            else:
                return False
        else:
            return True
    else:
        return False


if is_leap_year(1996):
    print("{0} is a leap year".format(1996))
else:
    print("{0} is NOT a leap year".format(1996))
