from string import ascii_letters

# this is a python3 implementation - str.maketrans (3) as opposed to string.maketrans (2)
# replace each ASCII letter letter with its corresponding one starting from the end
trantab = str.maketrans(ascii_letters, ascii_letters.lower()[::-1])


def decode(ciphered_text):
    return ''.join(char for char in ciphered_text if char.isalnum()).translate(trantab)


def encode(plain_text):
    plain_text = decode(plain_text)
    # result is written in groups of fixed length, the traditional group size being 5 letters, and punctuation is excluded.
    return ' '.join(plain_text[i:i+5] for i in range(0, len(plain_text), 5))
