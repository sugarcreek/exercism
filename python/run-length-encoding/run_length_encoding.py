from re import sub


def decode(string):
    if not string:
        return ''
    return sub(r'(\d+)(\D)', lambda m: m.group(2) * int(m.group(1)), string)


def encode(string):
    if not string:
        return ''
    # 1 liner if '2 1h1s2q1 1q2w2 ' is accepted for '2 hs2q q2w2 '
    # return sub(r'(.)\1*', lambda m: str(len(m.group(0))) + m.group(1), string)
    last_char = string[0]
    max_index = len(string)
    i = 1
    while i < max_index and last_char == string[i]:
        i += 1
    if i > 1:  # to avoid returning '2 1h1s2q1 1q2w2 ' instead of '2 hs2q q2w2 '
        return str(i) + last_char + encode(string[i:])
    else:
        return last_char + encode(string[i:])
