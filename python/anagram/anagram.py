def detect_anagrams(word, candidates):
    """Finds any words in candidates that are anagrams of word"""
    letters = sorted(word.lower())
    return [candidate
            for candidate in candidates
            if sorted(candidate.lower()) == letters and candidate.lower() != word.lower()]
