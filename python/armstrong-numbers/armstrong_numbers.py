def is_armstrong(number):
    orig_no = number
    length = len(str(number))
    try:
        sum = 0
        while number > 0:
            remainder = number % 10         # get the last digit
            sum += remainder ** length
            number //= 10                   # floor division - 5/2 = 2 (=== integer part of actual float)
        return sum == orig_no
    except ValueError:
        print("{} is not a valid number.".format(number))
